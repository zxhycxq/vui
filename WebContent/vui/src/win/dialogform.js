/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * DialogForm对话框,将Dialog和Form组合<br>
 * <pre>
win = new VUI.DialogForm({
	contentId:'win'
	,formId:'form'
	,title:'Dialog标题'
	,width:500
	,onOk:function() {
		alert('ok')
	}
});
 </pre>
 * @class VUI.DialogForm
 * @extends VUI.Dialog
 */
VUI.Class('DialogForm',{
	// 默认属性
	OPTS:{
	}
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
		this.form = new VUI.Form(opts);
	}
	/**
	 * 同Form.load()
	 * @see {VUI.Form.load}
	 */
	,load:function(data) {
		this.form.load(data);
	}
	/**
	 * 同Form.setData()
	 * @see {VUI.Form.setData}
	 */
	,setData:function(data) {
		this.load(data);
	}
	/**
	 * 同Form.getData()
	 * @see {VUI.Form.getData}
	 */
	,getData:function() {
		return this.form.getData();
	}
	/**
	 * 同Form.check()
	 * @see {VUI.Form.check}
	 */
	,check:function() {
		return this.form.check();
	}
	/**
	 * 同Form.validate()
	 * @see {VUI.Form.validate}
	 */
	,validate:function() {
		return this.form.validate();
	}
	/**
	 * 同Form.submit()
	 * @see {VUI.Form.submit}
	 */
	,submit:function(opts) {
		this.form.submit(opts);
	}
	/**
	 * 同Form.reset()
	 * @see {VUI.Form.reset}
	 */
	,reset:function() {
		this.form.reset();
	}
	/**
	 * 返回VUI.Form实例对象
	 */
	,getForm:function() {
		return this.form;
	}
},VUI.Dialog);

})();